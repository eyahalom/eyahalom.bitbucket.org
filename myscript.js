$(document).ready(function(){

	// Image descriptions
	descr = {
		china: 					"China by Nurit Zarchi, Miskal-Yedioth Ahronoth Books, 2016",
		kaleidoscope: 			"Kaleidoscope by Chava Nissimov, Miskal-Yedioth Ahronoth Books, 2015",
		the_lost_night_sleep: 	"The Lost Night Sleep by Ruth Oren, Hakibbutz Hameuchad Publishing House Ltd., 2015",
		in_another_country: 	"In Another Country by Tal Nitzan, Am Oved Publishers Ltd., 2013",
		badulina: 				"Badulina by Gabi Nitzan, Yoel Geva, 2011",
		where_to: 				'"Where To", Private Collection, 2016',
		kiki_coocoo_clocks: 	"Kiki CooCoo Clocks, Private Collection, 2016"
	};

	// Fancybox initialization

	$(".fancybox").fancybox({
		beforeLoad: function() {
			this.title = descr[$(this.element).attr('rel')];
		},
		padding: 10,
		nextEffect: "fade",
		nextSpeed: "slow",
		prevEffect: "fade",
		prevSpeed: "slow",
		helpers: {
			title: {
				type: "inside"
			},
			overlay: {
				locked: false
			}

		}
	});

	// Back-to-top button appearance

    $(window).scroll(function(){
        if ($(".main").offset().top - $(window).scrollTop() <= 50){
            $(".back-to-top").fadeIn(500);
        } else {
            $(".back-to-top").fadeOut(500);
        }

    });

    // Back-to-top button click

    $(".back-to-top").click(function(){
        $("html, body").animate({scrollTop:0}, 1000);
    });


});